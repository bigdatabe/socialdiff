package be.bigdata.socialdiff.bolt;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import be.bigdata.socialdiff.Fields;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;

public class TwitterFetchingBolt extends BaseBasicBolt {
	private static final String SOURCE = "twitter";
	private static final String URL_FORMAT = "http://www.twitter.com/%s";

	public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
		Object id = tuple.getValueByField("id");
		String username = tuple.getStringByField("twitter.username");

		String url = String.format(URL_FORMAT, username);

		try {
			Document document = Jsoup.parse(new URL(url), 10000);

			basicOutputCollector.emit(new Values(id, SOURCE, Fields.FULL_NAME, document.select("h3.fullname").text()));
			basicOutputCollector.emit(new Values(id, SOURCE, Fields.BIO, document.select("p.bio").text()));
			basicOutputCollector.emit(new Values(id, SOURCE, Fields.LOCATION, document.select("p.location-and-url span.location").text()));
			basicOutputCollector.emit(new Values(id, SOURCE, Fields.URL, document.select("p.location-and-url span.url a").attr("href")));

		} catch (IOException e) {
			basicOutputCollector.reportError(e);
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
		outputFieldsDeclarer.declare(new backtype.storm.tuple.Fields("id", "source", "field", "value"));
	}

}
