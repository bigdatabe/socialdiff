package be.bigdata.socialdiff;

public interface Fields {
	public static final String FULL_NAME = "fullname";
	public static final String BIO = "bio";
	public static final String LOCATION = "location";
	public static final String URL = "url";
}
