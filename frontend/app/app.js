
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes');

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/', routes.index);
app.post('/twitter', function(req, res){
	console.log(req.body.user);
	var twitter_username = req.param('twitter_username', null);
	res.render('index', { title: 'Twitter username '+ twitter_username})
});
app.post('/linkedin', function(req, res){
	console.log(req.body.user);
	var linkedin_username = req.param('linkedin_username', null);
	res.render('index', { title: 'Linkedin username '+ linkedin_username})
});
app.post('/facebook', function(req, res){
	console.log(req.body.user);
	var facebook_username = req.param('twitter_username', null);  
	res.render('index', { title: 'Facebook username '+ facebook_username})
});


app.listen(3000, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
