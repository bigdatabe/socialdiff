package be.bigdata.socialdiff.consumer.twitter;


import org.apache.commons.beanutils.BeanMap;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import java.util.Map;

public class TwitterProfileFetch {

    private Twitter twitter = connect();

    public Map fetch (String screenName) {
        try {
            User user = twitter.showUser(screenName);
            return new BeanMap(user);

        } catch (TwitterException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Twitter connect() {
        Configuration configuration = new ConfigurationBuilder()
                .build();

        return new TwitterFactory(configuration).getInstance();
    }
}