package be.bigdata.socialdiff;

import backtype.storm.transactional.TransactionalTopologyBuilder;
import storm.kafka.HostPort;
import storm.kafka.KafkaConfig;
import storm.kafka.TransactionalKafkaSpout;

import java.util.List;
import java.util.Map;

public class SocialLookupTopology {
	public TransactionalTopologyBuilder construct(Map<String, Object> properties) {
		KafkaConfig spoutConfig = new KafkaConfig(
				(List<HostPort>) properties.get("kafka.hosts"),
				1, "bigdata.social"
		);

		// -- start from latest
		spoutConfig.startOffsetTime = -1;

		TransactionalTopologyBuilder result = new TransactionalTopologyBuilder(
				"bigdata.social", "spout", new TransactionalKafkaSpout(spoutConfig)
		);

		return result;
	}
}
