package be.bigdata.socialdiff.consumer.twitter;


import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.Map;

public class TwitterProfileFetchBolt extends BaseBasicBolt {

    TwitterProfileFetch twitterProfileFetch = new TwitterProfileFetch();
    private static final String SOURCE = "twitter";

    @Override
    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        Object id = tuple.getValueByField("id");
        String username = tuple.getStringByField("twitter.username");
        Map twitterProfile = twitterProfileFetch.fetch(username);

        for (Object field : twitterProfile.keySet()) {
            Object value = twitterProfile.get(field);
            basicOutputCollector.emit(new Values(id, SOURCE, field.toString(), value.toString()));
        }

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new backtype.storm.tuple.Fields("id", "source", "field", "value"));
    }
}
