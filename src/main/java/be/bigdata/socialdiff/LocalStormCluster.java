package be.bigdata.socialdiff;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import com.google.common.collect.ImmutableList;
import org.apache.log4j.Logger;
import storm.kafka.HostPort;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class LocalStormCluster {
	private static final Logger LOGGER = Logger.getLogger(LocalStormCluster.class);

	private Map<String, Object> settings = new HashMap<String, Object>();

	private LocalCluster cluster = new LocalCluster();

	public static void main(String[] args) {
		final LocalStormCluster launcherLocal = new LocalStormCluster();

		try {
			launcherLocal.initialize(args);

			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				public void run() {
					try {
						launcherLocal.halt();
					} catch (Exception e) {
						LOGGER.warn("Unable to stop the proxy", e);
					}
				}
			}));

			launcherLocal.launch();
		} catch (Exception e) {
			LOGGER.error("Unable to launch", e);
			try {
				launcherLocal.halt();
			} catch (Exception e1) {
				LOGGER.error("Unable to stop the launcher", e1);
			}
		}
	}

	public void initialize(String[] args) throws Exception {
		settings.put("kafka.hosts", ImmutableList.of(new HostPort("localhost")));
	}

	public void launch() throws Exception {
		cluster = new LocalCluster();

		Config config = new Config();
//		config.setDebug(true);
		config.setMaxTaskParallelism(3);
		config.setMessageTimeoutSecs(5 * 60);

		loadSocialLookupTopology(config);

		waitOnExit();

		halt();
	}

	protected void loadSocialLookupTopology(Config config) {
		cluster.submitTopology(
				"bigdata.social", config,
				new SocialLookupTopology().construct(settings).buildTopology()
		);
	}

	public void waitOnExit() throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		System.out.println("Enter 'exit' to close");
		while (! "exit".equalsIgnoreCase(line = reader.readLine())) {
			Thread.sleep(500);
		}
	}

	public void halt() throws Exception {
		if (cluster != null) cluster.shutdown();
	}
}
